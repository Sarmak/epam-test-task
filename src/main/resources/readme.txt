1. this project is for java 8
2. you can run this project by using this command: java -jar initial-0.0.1.jar
3. getting a list of potato bags:
    GET http://localhost:8080/api/potato-bags?amount=5
4. adding a new potato bag:
    POST http://localhost:8080/api/potato-bags
    {
    	"amount": "72",
    	"supplier" : "De Coster",
    	"packedDate" : "2007-12-03T10:15:30",
    	"price": "20"
    }