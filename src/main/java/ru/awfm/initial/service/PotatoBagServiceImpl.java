package ru.awfm.initial.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.awfm.initial.entity.PotatoBagEntity;
import ru.awfm.initial.repository.PotatoBagRepository;

import java.util.List;

/**
 * Created by Kirill Bakhaev on 26.02.2018.
 */

@Service
public class PotatoBagServiceImpl implements PotatoBagService {

    private final PotatoBagRepository potatoBagRepository;

    @Autowired
    public PotatoBagServiceImpl(PotatoBagRepository potatoBagRepository) {
        this.potatoBagRepository = potatoBagRepository;
    }

    @Override
    public PotatoBagEntity save(PotatoBagEntity potatoBag) {
        return potatoBagRepository.save(potatoBag);
    }

    @Override
    public List<PotatoBagEntity> findAll(int amount) {
        Pageable pageRequest = new PageRequest(0, amount);
        return potatoBagRepository.findAll(pageRequest).getContent();
    }

    @Override
    public void deleteAll() {
        potatoBagRepository.deleteAll();
    }

    public boolean isCorrectSupplier(String supplier) {
        switch (supplier) {
            case "De Coster":
            case "Owel":
            case "Patatas Ruben":
            case "Yunnan Spices":
                return true;
            default:
                return false;
        }
    }
}
