package ru.awfm.initial.service;

import ru.awfm.initial.entity.PotatoBagEntity;

import java.util.List;

/**
 * Created by Kirill Bakhaev on 26.02.2018.
 */

public interface PotatoBagService {

    PotatoBagEntity save(PotatoBagEntity potatoBag);

    List<PotatoBagEntity> findAll(int amount);

    void deleteAll();

    boolean isCorrectSupplier(String supplier);

}
