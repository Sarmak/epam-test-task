package ru.awfm.initial.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.awfm.initial.entity.PotatoBagEntity;

/**
 * Created by Kirill Bakhaev on 26.02.2018.
 */

@Repository
public interface PotatoBagRepository extends JpaRepository<PotatoBagEntity, Long> {
}
