package ru.awfm.initial.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.awfm.initial.entity.PotatoBagEntity;
import ru.awfm.initial.service.PotatoBagService;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.List;

/**
 * Created by Kirill Bakhaev on 26.02.2018.
 */

@RestController
@RequestMapping(value="/api/potato-bags")
public class PotatoBagController {

    private final PotatoBagService potatoBagService;

    @Autowired
    public PotatoBagController(PotatoBagService potatoBagService) {
        this.potatoBagService = potatoBagService;
    }

    @GetMapping
    public List<PotatoBagEntity> findAll(@RequestParam(value="amount", required = false, defaultValue = "3") int amount) {
        return potatoBagService.findAll(amount);
    }

    @PostMapping
    public PotatoBagEntity save(@Valid @RequestBody PotatoBagEntity potatoBag) {
        if (potatoBagService.isCorrectSupplier(potatoBag.getSupplier()))
            return potatoBagService.save(potatoBag);
        else throw new ValidationException("Supplier of a bag must be one of \"De Coster\", \"Owel\", " +
                "\"Patatas Ruben\",\"Yunnan Spices\"");
    }

    //added for testing purposes
    @GetMapping(value="/delete")
    public void deleteAll() {
        potatoBagService.deleteAll();
    }

}
