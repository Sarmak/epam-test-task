package ru.awfm.initial.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ru.awfm.initial.util.JsonDateTimeDeserializer;
import ru.awfm.initial.util.JsonDateTimeSerializer;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Created by Kirill Bakhaev on 26.02.2018.
 */

@Entity
@Table(name = "potato_bags")
public class PotatoBagEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    @Min(1)
    @Max(100)
    @NotNull
    private Byte amount;
    //(must be one of "De Coster", "Owel", "Patatas Ruben","Yunnan Spices")
    private String supplier;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
    private LocalDateTime packedDate;
    @Min(1)
    @Max(50)
    @NotNull
    private Byte price;


    public PotatoBagEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Byte getAmount() {
        return amount;
    }

    public void setAmount(Byte amount) {
        this.amount = amount;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public LocalDateTime getPackedDate() {
        return packedDate;
    }

    public void setPackedDate(LocalDateTime packedDate) {
        this.packedDate = packedDate;
    }

    public Byte getPrice() {
        return price;
    }

    public void setPrice(Byte price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PotatoBagEntity that = (PotatoBagEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        if (supplier != null ? !supplier.equals(that.supplier) : that.supplier != null) return false;
        if (packedDate != null ? !packedDate.equals(that.packedDate) : that.packedDate != null) return false;
        return price != null ? price.equals(that.price) : that.price == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (supplier != null ? supplier.hashCode() : 0);
        result = 31 * result + (packedDate != null ? packedDate.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PotatoBagEntity{" +
                "id='" + id + '\'' +
                ", amount=" + amount +
                ", supplier='" + supplier + '\'' +
                ", packedDate=" + packedDate +
                ", price=" + price +
                '}';
    }
}
